package com.cdac.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cdac.pojo.Customer;

public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private String authorOfServlet;

	public BookServlet() {
		System.out.println("Parameterless Constructor of " + this.getClass().getName());
	}

	@Override
	public void init() throws ServletException {
		System.out.println("Init Method : " + this.getClass().getName());

		/*
		 * Initialize authorOfServlet variable from ServletConfig
		 */
		ServletConfig servletConfig = this.getServletConfig();
		authorOfServlet = servletConfig.getInitParameter("author");

		System.out.println("Author Of Book Servlet : " + this.authorOfServlet);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");

		try (PrintWriter pw = response.getWriter()) {

			/*
			 * Get the HttpSession object
			 */
			HttpSession httpSession = request.getSession();

			/*
			 * Check Session is new or old
			 */
			System.out.println(httpSession.isNew());

			/*
			 * get JSESSION Id
			 */
			System.out.println(httpSession.getId());

			/*
			 * Add customer object into session object
			 */
			Customer customer = (Customer) httpSession.getAttribute("customer_details");

			pw.println("Welcome " + customer.getEmail() + "<br>");

			pw.print("List of Books will be Displayed Here <br>");
			pw.print("<a href='logout'>Logout</a>");
		} catch (Exception e) {
			throw new ServletException("Error in doGet() method of " + this.getClass().getName());
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
