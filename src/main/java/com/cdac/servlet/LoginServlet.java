package com.cdac.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cdac.dao.CustomerDao;
import com.cdac.dao.CustomerDaoImpl;
import com.cdac.pojo.Customer;

@WebServlet(urlPatterns = "/authenticate", loadOnStartup = 1, initParams = {
		@WebInitParam(name = "author", value = "Shivam Palaskar") })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	CustomerDao customerDao = null;

	/*
	 * This property of LoginServlet is initialized at the time of object creation
	 * but not in the constructor rather in init() method
	 */
	private String authorOfServlet;

	public LoginServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {
		System.out.println("Init Method : " + this.getClass().getName());

		/*
		 * Initialize ServletSpecific variables from servletConfig
		 */
		ServletConfig servletConfig = this.getServletConfig();
		
		System.out.println(servletConfig.getClass().getName()); // org.apache.catalina.core.StandardWrapperFacade
		
		this.authorOfServlet = servletConfig.getInitParameter("author");

		System.out.println("Author of LoginServlet : " + this.authorOfServlet);

		/*
		 * We don't have to handle exception here only, we throw it, So that Web
		 * container should come to know that init() method has failed.
		 */
		try {
			customerDao = new CustomerDaoImpl();
			System.out.println("Initialized Customer Dao");
		} catch (ClassNotFoundException | SQLException e) {
			/*
			 * Centralized exception handling by throwing exception
			 */
			throw new ServletException("Error in CustomerDao inialization", e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		Customer customer = null;
		try {
			customer = customerDao.authenticateCustomer(email, password);
		} catch (Exception e) {
			throw new ServletException("Error in " + this.getClass().getName(), e);
		}

		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {

			if (customer == null)
				pw.println("Invalid Login. " + "<a href='login.html'>Retry</a>");
			else {

				/*
				 * Get the HttpSession object
				 */
				HttpSession httpSession = request.getSession();

				/*
				 * Set Maximum session inactive interval time for 10 seconds
				 * 
				 * So if there is no any new request for 10 seconds then httpSession object will
				 * get deleted from server heap automatically
				 */
				httpSession.setMaxInactiveInterval(60);

				/*
				 * Check Session is new or old
				 */
				System.out.println(httpSession.isNew());

				/*
				 * get JSESSION Id
				 */
				System.out.println(httpSession.getId());

				/*
				 * Add customer object into session object
				 */
				httpSession.setAttribute("customer_details", customer);

				System.out.println(httpSession.getClass().getName()); // org.apache.catalina.session.StandardSessionFacade

				response.sendRedirect("books");
				/*
				 * Status Code : 302
				 * 
				 * location = http://host:port/servletsessionmanagement2/books
				 * 
				 * set-cookie : cookie
				 * 
				 * - name : JSESSIONID
				 * 
				 * - value : kjhaiu9837haf8763
				 * 
				 * details | body EMPTY
				 */
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	public void destroy() {
		try {
			customerDao.cleanUp();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error in destroy() " + this.getClass().getName(), e);
		}

	}

}
